# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|N|
|Basic rules|20%|N|
|Jucify mechanisms|15%|N|
|Animations|10%|N|
|Particle Systems|10%|N|
|UI|5%|N|
|Sound effects|5%|N|
|Leaderboard|5%|N|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms : level,skill
2. Animations : player has animation
3. Particle Systems : explosion special effect
4. Sound effects : 2 sounds, change volume(by Q、W)
5. Leaderboard : NO

# Bonus Functions Description : 
1. player health:有幾命會顯示在左上角
2. Ultimate skill number or power : 顯示生命的下面會顯示大絕的數量，到0就沒有大絕，每3秒會+5
3. 按Q變大聲，按W變小聲
4. 每過一關敵人的生命會+1，代表要打更多次才會死，敵人發射子彈的頻率也會變高
5. 沒有爆開的粒子效果，但有爆炸的動畫效果
