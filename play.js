var explosion;
var bubble;
var player;
var aliens;
var bullets;
var bulletTime = 0;
var ultimatebullets;
var ultimatebulletTime = 0;
var cursors;
var fireButton;
var ultimatebutton;
var explosions;
var starfield;
var score = 0;
var scoreString = '';
var scoreText;
var ultimatecnt = 20;
var ultimateString = '';
var ultimateText;
var lives;
var enemyBullet;
var firingTimer = 0;
var ultimateTimer = 0
var stateText;
var livingEnemies = [];
var hardlevel = 0;

var playState = {
create: function() {
    game.physics.startSystem(Phaser.Physics.ARCADE);
    // sound
    explosion = game.add.audio('explosion');
    bubble = game.add.audio('bubble');
    explosion.volume = 0.5;
    bubble.volume = 0.5;
    small = game.input.keyboard.addKey(Phaser.Keyboard.W);
    big = game.input.keyboard.addKey(Phaser.Keyboard.Q);
    //  The scrolling starfield background
    starfield = game.add.tileSprite(0, 0, 800, 600, 'starfield');

    //  Our bullet group
    bullets = game.add.group();
    bullets.enableBody = true;
    bullets.physicsBodyType = Phaser.Physics.ARCADE;
    bullets.createMultiple(30, 'bullet');
    bullets.setAll('anchor.x', 0.5);
    bullets.setAll('anchor.y', 1);
    bullets.setAll('outOfBoundsKill', true);
    bullets.setAll('checkWorldBounds', true);
    //  Our ultimatebullet group
    ultimatebullets = game.add.group();
    ultimatebullets.enableBody = true;
    ultimatebullets.physicsBodyType = Phaser.Physics.ARCADE;
    ultimatebullets.createMultiple(30, 'ultimateBullet');
    ultimatebullets.setAll('anchor.x', 0.5);
    ultimatebullets.setAll('anchor.y', 1);
    ultimatebullets.setAll('outOfBoundsKill', true);
    ultimatebullets.setAll('checkWorldBounds', true);

    // The enemy's bullets
    enemyBullets = game.add.group();
    enemyBullets.enableBody = true;
    enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
    enemyBullets.createMultiple(30, 'enemyBullet');
    enemyBullets.setAll('anchor.x', 0.5);
    enemyBullets.setAll('anchor.y', 1);
    enemyBullets.setAll('outOfBoundsKill', true);
    enemyBullets.setAll('checkWorldBounds', true);

    //  The hero!
    player = game.add.sprite(400, 500, 'ship');
    player.anchor.setTo(0.5, 0.5);
    game.physics.enable(player, Phaser.Physics.ARCADE);
    player.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
    player.animations.play('fly');

    //  The baddies!
    aliens = game.add.group();
    aliens.enableBody = true;
    aliens.physicsBodyType = Phaser.Physics.ARCADE;

    this.createAliens();

    //  The score
    scoreString = 'Score : ';
    scoreText = game.add.text(10, 10, scoreString + score, { font: '34px Arial', fill: '#fff' });

    //  Lives
    lives = game.add.group();
    game.add.text(game.world.width - 150, 10, 'Lives : ', { font: '34px Arial', fill: '#fff' });

    //  ultimate skills
    ultimateString = 'Ulti : '
    ultimateText = game.add.text(game.world.width - 150, 80, ultimateString + ultimatecnt, { font: '34px Arial', fill: '#fff' });
    //  Text
    stateText = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '84px Arial', fill: '#fff' });
    stateText.anchor.setTo(0.5, 0.5);
    stateText.visible = false;

    for (var i = 0; i < 3; i++) 
    {
        var ship = lives.create(game.world.width - 100 + (30 * i), 60, 'ship');
        ship.anchor.setTo(0.5, 0.5);
        ship.angle = 90;
        ship.alpha = 0.4;
    }

    //  An explosion pool
    explosions = game.add.group();
    explosions.createMultiple(30, 'kaboom');
    explosions.forEach(this.setupInvader, this);

    //  And some controls to play the game with
    cursors = game.input.keyboard.createCursorKeys();
    fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    ultimatebutton = game.input.keyboard.addKey(Phaser.Keyboard.M);
},

createAliens :function() {
    for (var y = 0; y < 4; y++)
    {
        for (var x = 0; x < 10; x++)
        {
            var alien = aliens.create(x * 48, y * 50, 'invader');
            alien.anchor.setTo(0.5, 0.5);
            alien.body.moves = false;
            alien.live = hardlevel;
        }
    }
    aliens.x = 100;
    aliens.y = 50;

    //  All this does is basically start the invaders moving. Notice we're moving the Group they belong to, rather than the invaders directly.
    var tween = game.add.tween(aliens).to( { x: 200 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);

    //  When the tween loops it calls descend
    tween.onLoop.add(this.descend, this);
},

setupInvader :function (invader) {
    invader.anchor.x = 0.5;
    invader.anchor.y = 0.5;
    invader.animations.add('kaboom');
},
descend:function () {
    aliens.y += 10;
},
update :function () {

    //  Scroll the background
    starfield.tilePosition.y += 2;

    if (player.alive)
    {
        //  Reset the player, then check for movement keys
        player.body.velocity.setTo(0, 0);

        if (cursors.left.isDown && player.x > 20) player.body.velocity.x = -200;
        if (cursors.right.isDown && player.x < 580) player.body.velocity.x = 200;
        if (cursors.up.isDown && player.y > 20) player.body.velocity.y = -200;
        if (cursors.down.isDown && player.y < 580) player.body.velocity.y = 200;
        //control volume
        if (small.isDown && bubble.volume >0){
            bubble.volume -= 0.1;
            explosion.volume -= 0.1;
        }
        if (big.isDown && bubble.volume <1){
            bubble.volume += 0.1;
            explosion.volume += 0.1;
        }
        //  Firing?
        if (fireButton.isDown){
            bubble.play();
            this.fireBullet();
        }
        if (ultimatebutton.isDown){
            explosion.play();
            this.fireultimate();
        }
        if(game.time.now > ultimateTimer)
        {   
            ultimatecnt =  ultimatecnt + 5;
            ultimateText.text = ultimateString + ultimatecnt;
            ultimateTimer = game.time.now + 3000;
        }
        if (game.time.now > firingTimer)
        {
            this.enemyFires();
        }
        //  Run collision
        game.physics.arcade.overlap(bullets, aliens, this.collisionHandler, null, this);
        game.physics.arcade.overlap(ultimatebullets, aliens, this.ultcollisionHandler, null, this);
        game.physics.arcade.overlap(enemyBullets, player, this.enemyHitsPlayer, null, this);
    }

},
ultcollisionHandler :function (bullet,alien) {
    if(alien.live > 1)  alien.live -= 1;
    else {
        alien.kill();
        score += 20;
        scoreText.text = scoreString + score;
        var explo = explosions.getFirstExists(false);
        explo.reset(alien.body.x, alien.body.y);
        explo.play('kaboom', 30, false, true);
    }

    if (aliens.countLiving() == 0)
    {
        score += 1000;
        scoreText.text = scoreString + score;
        enemyBullets.callAll('kill',this);
        stateText.text = " WIN!!!, \n Click to restart";
        hardlevel = hardlevel + 1;
        stateText.visible = true;
        game.global.score = score;
        game.input.onTap.addOnce(this.restart,this);
    }
},
collisionHandler:function (bullet, alien) {
    bullet.kill();
    if(alien.live == 0){
        alien.kill();
        score += 20;
        scoreText.text = scoreString + score;
        var explo = explosions.getFirstExists(false);
        explo.reset(alien.body.x, alien.body.y);
        explo.play('kaboom', 30, false, true);
    }
    else alien.live -= 1; 


    if (aliens.countLiving() == 0)
    {
        score += 1000;
        scoreText.text = scoreString + score;
        enemyBullets.callAll('kill',this);
        stateText.text = " WIN!!!, \n Click to restart";
        hardlevel = hardlevel + 1;
        stateText.visible = true;
        game.global.score = score;
        game.input.onTap.addOnce(this.restart,this);
    }

},
enemyHitsPlayer :function (player,bullet) {
    bullet.kill();
    live = lives.getFirstAlive();
    if (live)
    {
        live.kill();
    }
    //  And create an explosion :)
    var explosion = explosions.getFirstExists(false);
    explosion.reset(player.body.x, player.body.y);
    explosion.play('kaboom', 30, false, true);
    // When the player dies
    if (lives.countLiving() < 1)
    {
        player.kill();
        enemyBullets.callAll('kill');
        stateText.text=" GAME OVER \n Click to restart";
        stateText.visible = true;
        hardlevel = 1;
        //the "click to restart" handler
        game.global.score = score;
        game.input.onTap.addOnce(this.restart,this);
    }

},

enemyFires:function  () {
    //  Grab the first bullet we can from the pool
    enemyBullet = enemyBullets.getFirstExists(false);
    livingEnemies.length=0;
    aliens.forEachAlive(function(alien){
        // put every living enemy in an array
        livingEnemies.push(alien);
    });
    if (enemyBullet && livingEnemies.length > 0)
    {      
        var random=game.rnd.integerInRange(0,livingEnemies.length-1);
        // randomly select one of them
        var shooter=livingEnemies[random];
        // And fire the bullet from this enemy
        enemyBullet.reset(shooter.body.x, shooter.body.y);
        game.physics.arcade.moveToObject(enemyBullet,player,120);
        if(hardlevel<5)
        firingTimer = game.time.now + 2000 - 350*hardlevel;
        else  firingTimer = game.time.now + 300;
    }
},
fireBullet:function  () {
    //  To avoid them being allowed to fire too fast we set a time limit
    if (game.time.now > bulletTime)
    {
        //  Grab the first bullet we can from the pool
        bullet = bullets.getFirstExists(false);
        if (bullet)
        {
            //  And fire it
            bullet.reset(player.x, player.y + 8);
            bullet.body.velocity.y = -400;
            bulletTime = game.time.now + 200;
        }
    }

},
fireultimate :function() {
    //  To avoid them being allowed to fire too fast we set a time limit
    if(ultimatecnt>0){
        ultimatecnt -= 1;
        ultimateText.text = ultimateString + ultimatecnt;
        if (game.time.now > ultimatebulletTime)
        {
            //  Grab the first bullet we can from the pool
            ultimatebullet = ultimatebullets.getFirstExists(false);
            if (ultimatebullet)
            {
                //  And fire it
                ultimatebullet.reset(player.x, player.y + 8);
                ultimatebullet.body.velocity.y = -400;
                ultimatebulletTime = game.time.now + 200;
            }
        }
    }

},
render:function () {
    // for (var i = 0; i < aliens.length; i++)
    // {
    //     game.debug.body(aliens.children[i]);
    // }
},
resetBullet:function (bullet) {
    //  Called if the bullet goes out of the screen
    bullet.kill();
},
resetultimateBullet :function (ultimatebullet) {
    //  Called if the bullet goes out of the screen
    ultimatebullet.kill();
},

restart :function() {
    game.state.start('menu'); 
}, 
    /*
    //  A new level starts 
    //resets the life count
    lives.callAll('revive');
    //  And brings the aliens back from the dead :)
    aliens.removeAll();
    createAliens();
    //revives the player
    player.revive();
    //hides the text
    stateText.visible = false;
    */
}
